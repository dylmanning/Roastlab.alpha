var express = require('express');
var session = require('express-session');
var passport = require('passport');
var localStrategy = require('passport-local');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var db = mongoose.connect('mongodb://localhost/roastlab')
  , User = require('./models/user')
  , Bean = require('./models/bean');

var app = express();

//passport config
passport.use(new localStrategy(
  function(username, password, done) {
    User.findOne({ username: username, password: password }, function(err, user){
      if(user)
        return done(null, user);
      else
        return done(null, false);
    });
}));

passport.serializeUser(function(user, done){ done(null, user); });
passport.deserializeUser(function(user, done) { done(null, user); });


//middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: '5eQ443gMkl1' }));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', express.static(__dirname + '/public'));

//api
app.post('/api/login', passport.authenticate('local'), function(req, res){ res.sendStatus(200); });

app.get('/api/loggedin', function(req, res) { res.send(req.isAuthenticated() ? req.user : '0'); });

app.post('/api/logout', function(req, res) { req.logout(); res.sendStatus(200); });

app.get('/api/beans', function(req, res) {
  Bean.find({}, function(err, docs) {
    res.json(docs);
  });
});

app.get('/api/bean/:id', function(req, res) {
  Bean.findById(req.params.id, function(err, docs) {
    res.json(docs);
  });
});

app.delete('/api/bean/:id', function(req, res) {
  Bean.findByIdAndRemove(req.params.id, function(err, docs) {
    res.json(docs);
  });
});

app.post('/api/bean', function(req, res) {
  var newBean = new Bean({
    name: req.body.name,
    type: req.body.type,
    added: req.body.added,
    addedBy: req.body.addedBy,
    origin: req.body.origin,
    farmer: req.body.farmer
  });
  newBean.save();
  res.sendStatus(200);
});

app.put('/api/bean/state/:id', function(req, res) {

  Bean.findOneAndUpdate(req.params.id, { $set: { assesmentState: req.body.assesmentState } }, function(err, docs) {
    res.sendStatus(200);
  });

  Bean.findById(req.params.id, function(err, docs) {

    var newAssesment = {
      name: 'Next Tab',
      added: new Date(),
      addedBy: req.body.addedBy
    };

    docs.assesment.push(newAssesment);
    docs.save();
  });
});

//app.get('/*', function(req, res) { res.redirect('/'); });

app.listen(3000);
