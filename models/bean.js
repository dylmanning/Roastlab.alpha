var mongoose = require('mongoose');

var beanSchema = mongoose.Schema({
  name: String,
  type: String,
  added: {
    type: Date,
    default: Date.now
  },
  addedBy: String,
  updated: {
    type: Date,
    default: Date.now
  },
  updatedBy: String,
  origin: String,
  farmer: String,
  assesmentState: {
    type: Number,
    default: 1
  },
  assesment: [{
    name: String,
    added: {
      type: Date,
      default: Date.now
    },
    addedBy: String,
    comments: [{
      added: {
        type: Date,
        default: Date.now
      },
      body: String
    }]
  }],
});

var Bean = mongoose.model('Bean', beanSchema);

module.exports = Bean;
