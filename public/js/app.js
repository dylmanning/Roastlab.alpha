var app = angular.module('roastlab', ['ngRoute', 'ngAnimate']);

app.config(function($routeProvider, $locationProvider, $httpProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'views/dashboard.html',
    controller: 'dashController',
    //resolve: {
    //  check: Auth
    //}
  })
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'loginController'
  })
  .otherwise({
    redirectTo: '/'
  });

  $locationProvider.html5Mode(true);
  $httpProvider.useApplyAsync(true);

});

var Auth = function($q, $timeout, $http, $location, $rootScope) {
  var deferred = $q.defer();
  $http.get('/api/loggedin').success(function(user) {
    $rootScope.currentUser = user;

    //authenticated
    if(user !== '0')
      deferred.resolve();

    //not authenticated
    else {
      deferred.reject();
      $location.path('/login');
    }

  });
  return deferred.promise;
};

app.controller('loginController', function($scope, $http, $location, $timeout) {

  $scope.login = function(user) {
    $http.post('/api/login', user).success(function(res) {
      $scope.loginState = {};
      $location.path('/');
    })
    .error(function(res) {

      //wrong user/password
      if(res == 'Unauthorized')
        $scope.loginState = { class: 'alert-danger fadeIn', message: 'username/password is incorrect.' };

      //incomplete fields
      if(res == 'Bad Request')
        $scope.loginState = { class: 'alert-warning fadeIn', message: 'please fill out all the fields.' };

    });
  };

});

app.controller('dashController', function($scope, $http, $location, $route, $timeout, $rootScope) {

  $rootScope.currentDate = new Date();
  $scope.bean = {};
  $scope.tab = 1;

  $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };

  $scope.logout = function() {
    $http.post('/api/logout').success(function(res) {
      $rootScope.currentUser = {};
      $route.reload();
    });
  };

  $scope.filters = { type: '', origin: '' };
  $scope.clearFilter = function() { $scope.filters = {}; };
  $scope.setType = function(type) { $scope.filters.type = type; };
  $scope.setOrigin = function(origin) { $scope.filters.origin = origin; };

  $scope.getBeans = function() { $http.get('/api/beans').success($scope.renderBeans); };
  $scope.renderBeans = function(res) { $scope.beans = res; };

  $scope.getBean = function(id) {
    $scope.currentBean = null;
    $http.get('/api/bean/' + id).success(function(res) {
        $scope.currentBean = res;
    });
  };

  $scope.createBean = function(data) {
    data.added = new Date();
    data.addedBy = $scope.currentUser;

    $http.post('/api/bean', data).success(function(res) {
      $route.reload();
    });
  };

  $scope.removeBean = function(id) {
   $http.delete('/api/bean/' + id).success(function(res) {
     $scope.getBeans();
   });
  };

  $scope.upState = function(id, data) {
    console.log(data);

    data.added = new Date();
    data.addedBy = $scope.currentUser;
    data.assesmentState++;
    $scope.tab = data.assesmentState;

    $http.put('/api/bean/state/' + id, data).success(function(res) {
      $scope.getBean(id);
    });
  };

  $scope.downState = function(id, data) {
    data.assesmentState--;
    $http.put('/api/bean/state/' + id, data).success(function(res) {
      $scope.getBean(id);
    });
  };

  $scope.getBeans();

});
